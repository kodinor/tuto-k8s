package com.cheese.supplier;

import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheeseSupplierController {
	
	@Autowired
	private AtomicBoolean cheeseStatus;

	@GetMapping("/supplied")
	public ResponseEntity<String> supplyCheese() {
		
		if (!cheeseStatus.get()) {
			throw new IllegalStateException();
		}
		
		System.out.println("200 - Success Request");
		return ResponseEntity.ok("La vache qui rit");
	}
}
