package com.cheese.supplier;

import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CheeseSupplierAppApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CheeseSupplierAppApplication.class);
	
	@Bean
	public AtomicBoolean cheeseStatus() {
		return new AtomicBoolean(false);
	}

	@Bean
	public CommandLineRunner cheesePreparation(AtomicBoolean cheeseStatus) {
		return args -> {
			// Simulating a long statup async process
			Thread cheesePreparer = new Thread(() -> {				
				try {
					Thread.sleep(2_000);
					LOGGER.info("...");					
					Thread.sleep(2_000);
					LOGGER.info("Sofort");
					Thread.sleep(5_000);
					LOGGER.info("Just a sec.");
					Thread.sleep(3_000);
					LOGGER.info("One minute!");
					Thread.sleep(25_000);
					LOGGER.info("There you are");
					
					System.out.println("Service Ready");
					cheeseStatus.set(true);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});

			cheesePreparer.start();
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(CheeseSupplierAppApplication.class, args);
		System.out.println("Cheese-app started!");
	}
	

}
