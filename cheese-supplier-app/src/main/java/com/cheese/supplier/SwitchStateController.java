package com.cheese.supplier;

import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SwitchStateController {
	// Convenient class to simulate health/readiness of a service
	
	@Autowired
	private AtomicBoolean cheeseStatus;
	
	@GetMapping("/switch")
	public ResponseEntity<String> reset() {
		
		cheeseStatus.set(!cheeseStatus.get());
		return ResponseEntity.ok(cheeseStatus.get() ? "Ready" : "Not ready");
	}

}
