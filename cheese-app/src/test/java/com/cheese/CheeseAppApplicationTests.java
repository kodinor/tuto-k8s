package com.cheese;


import static org.junit.Assert.assertEquals;

import java.nio.file.Path;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.builder.ImageFromDockerfile;

public class CheeseAppApplicationTests {
	
	static Logger logger = LoggerFactory.getLogger(CheeseAppApplicationTests.class);

	static GenericContainer<?> cheeseAppContainer;
	
	RestTemplate restTemplate = new RestTemplate();

	@SuppressWarnings("resource")
	@BeforeClass
	public static void initImageAndcontainer() {
		
		ImageFromDockerfile cheeseAppDockerImage = new ImageFromDockerfile()
				.withDockerfile(Path.of("./Dockerfile"));
		
		cheeseAppContainer = new GenericContainer<>(cheeseAppDockerImage)
				.withFileSystemBind("store/cheese.conf", "/home/cheese.conf", BindMode.READ_ONLY)
				.withExposedPorts(8080) // Port(s) of app inside the container
				.withEnv("CONFIGURED_CHEESE", "Edamer")
				.withEnv("SECRET_CHEESE", "T2JhdHpkYQo=")
				.withLogConsumer(new Slf4jLogConsumer(logger));
		
		cheeseAppContainer.start();
	}

	@Test
	public void testConfiguredCheese() {

		int mappedPortFor8080 = cheeseAppContainer.getMappedPort(8080);
		String configuredCheese = restTemplate.getForObject("http://localhost:" + mappedPortFor8080 + "/configured",
				String.class).trim();
		
		assertEquals("Edamer", configuredCheese);
	}
	
	@Test
	public void testSecretCheese() {

		int mappedPortFor8080 = cheeseAppContainer.getMappedPort(8080);
		
		String secret = restTemplate.getForObject("http://localhost:" + mappedPortFor8080 + "/secret",
				String.class).trim();
		
		assertEquals("Obatzda", secret);
	}
	
	@Test
	public void testStoredCheese() {

		int mappedPortFor8080 = cheeseAppContainer.getMappedPort(8080);
		
		String stored = restTemplate.getForObject("http://localhost:" + mappedPortFor8080 + "/stored",
				String.class).trim();
		
		assertEquals("Stored cheese is: Handkäs", stored);
	}

	// Not needed:
//	@After
//	public void chutdown() {
//		cheeseAppContainer.stop();
//	}
	
}








/*
 * Pros
 * - Test on a 100% similar environment as in prod.
 * - Write test environment (container infrastructure) once for all the tests, for all the team.
 * - No Mocking: Tests can be inaccurate if environment not correctly mocked
 * - No Mocking: Writing mocks is time consuming
 * 
 * Cons
 * - CPU! Memory! Especially on Jenkins Server.
 * - Creation of all the container/infrastructure, even for few tests
 * 
 * - (For container-based apps obviously)
 */
