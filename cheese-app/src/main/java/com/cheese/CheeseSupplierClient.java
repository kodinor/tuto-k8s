package com.cheese;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CheeseSupplierClient {
	
	@Value("${cheese.supplier.host}")
	private String supplierHost;

	private RestTemplate restTemplate = new RestTemplate();
	
	public String getSuppliedCheese() {
		System.out.println("Connecting to host: " + supplierHost);
		return restTemplate.getForObject("http://" + supplierHost + ":8081/supplied", String.class);
	}
	
}
