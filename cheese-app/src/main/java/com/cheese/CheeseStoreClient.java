package com.cheese;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CheeseStoreClient {
	
	@Value("${cheese.store.path}")
	private String cheesePath;

	public String getStoredCheese() throws IOException {
		System.out.println("Path is: " + cheesePath);
		File f = new File(cheesePath);
		if (!f.exists()) {
			return "not found";
		}
		
		return Files.readString(Path.of(cheesePath));
	}
}
