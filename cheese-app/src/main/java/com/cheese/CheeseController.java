package com.cheese;

import java.io.IOException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheeseController {
	
	private CheeseSupplierClient supplierClient;
	
	private CheeseStoreClient storeClient;

	@Autowired
	public CheeseController(CheeseSupplierClient supplierClient, CheeseStoreClient storeClient) {
		this.supplierClient = supplierClient;
		this.storeClient = storeClient;
	}

	@Value("${configured.cheese}")
	private String configuredCheese;
	
	@Value("${secret.cheese}")
	private String secretCheese;
	
	@GetMapping("/configured")
	public String getSelectedCheese() {
		return configuredCheese;
	}
	
	@GetMapping("/supplied")
	public String getSuppliedCheese() {
		return "Supplied cheese is: " + supplierClient.getSuppliedCheese();
	}
	
	@GetMapping("/secret")
	public String getSecretCheese() {
		System.out.println("Read secret: " + secretCheese);
		return new String(Base64.getDecoder().decode(secretCheese));
	}
	
	@GetMapping("/secret2")
	public String getSecretCheese2() {
		System.out.println("Read secret: " + secretCheese);
		return secretCheese;
	}
	
	@GetMapping("/stored")
	public String getStoredCheese() throws IOException {
		return "Stored cheese is: " + storeClient.getStoredCheese();
	}
	
}
